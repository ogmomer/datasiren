{{ config({
        "materialized": "incremental",
        "full_refresh": "false"
    })
}}

{% for sensor in dbt_utils.get_relations_by_prefix(var('datasiren:schema_name'), 'datasiren_', exclude='datasiren_audit_results', database=this.database) %}

    SELECT * 
    FROM {{ sensor }}

    {% if is_incremental() %}
        WHERE time_detected > 
          (
            SELECT MAX(time_detected) 
            FROM {{ this }} 
            WHERE sensor_name in (
              SELECT DISTINCT sensor_name 
              FROM {{ sensor }}
            )
          ) 
    {% endif %}

    {% if not loop.last %}
        UNION ALL
    {% endif %}
    

{% endfor -%}
