{% macro email_column_name_sensor(database) %}

    {{ adapter.dispatch('find_columns_with_names_like', ['datasiren'])('email_column_name_sensor', '%email%', database | trim ) }}

{%- endmacro -%}